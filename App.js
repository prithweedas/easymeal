import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import firebase from './src/config/firebaseConfig'
import App from './src'

export default App

import { createSwitchNavigator } from 'react-navigation'

import AuthLoadingScreen from './screens/AuthLoadingScreen'
import UnauthAppStack from './UnauthApp'
import AuthAppStack from './Authapp'

export default createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    UnauthApp: UnauthAppStack,
    AuthApp: AuthAppStack
  },
  {
    initialRouteName: 'AuthLoading'
  }
)

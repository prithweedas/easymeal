import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#6DB3BF',
    justifyContent: 'center',
    alignItems: 'center'
  },
  input: {
    width: '90%',
    paddingTop: '3%',
    paddingBottom: '3%',
    paddingLeft: '4%',
    paddingRight: '4%'
  }
})

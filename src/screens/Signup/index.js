import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
  StatusBar
} from 'react-native'

import styles from './styles'
import firebase from '../../config/firebaseConfig'
import Input from '../../Components/Input'
import SubmitButton from '../../Components/Button'

class SignupScreen extends React.Component {
  static navigationOptions = {
    header: null
  }

  state = {
    email: '',
    password: '',
    error: null,
    loading: false
  }

  handelInputChange = (field, text) => this.setState({ [field]: text })

  signup = () => {
    this.setState({ error: null, loading: true })
    const { email, password } = this.state
    if (email === '' && password === '')
      return this.setState({
        loading: false,
        error: 'Email & Password are required'
      })
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(data => console.log(data))
      .catch(err => {
        console.log(err)
        this.setState({ loading: false, error: err.message })
      })
  }
  render() {
    return this.state.loading ? (
      <View
        style={{
          backgroundColor: '#6DB3BF',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <StatusBar backgroundColor="#6DB3BF" barStyle="light-content" />

        <ActivityIndicator color="#fff" size="large" />
      </View>
    ) : (
      <KeyboardAvoidingView style={styles.container}>
        <StatusBar backgroundColor="#6DB3BF" barStyle="light-content" />
        <View
          style={{
            height: 10,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 10,
            alignContent: 'center',
            width: '100%'
          }}
        >
          {this.state.error && (
            <Text style={{ width: '90%', color: 'red', textAlign: 'center' }}>
              {this.state.error}
            </Text>
          )}
        </View>
        <Input
          field="email"
          handelChange={this.handelInputChange}
          style={styles.input}
          placeholder="Email"
          underlineColorAndroid="transparent"
          value={this.state.email}
        />
        <Input
          field="password"
          handelChange={this.handelInputChange}
          style={styles.input}
          placeholder="Password"
          underlineColorAndroid="transparent"
          secureTextEntry
          value={this.state.password}
        />
        <SubmitButton
          style={{ marginTop: 5 }}
          text="Sign Up"
          onPress={this.signup}
        />
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            marginTop: 10,
            borderBottomColor: '#fff',
            borderBottomWidth: 1
          }}
          onPress={() => this.props.navigation.push('Login')}
        >
          <Text style={{ color: '#fff' }}>Already have an account? Login</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    )
  }
}

export default SignupScreen

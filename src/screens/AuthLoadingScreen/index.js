import React from 'react'
import { ActivityIndicator, View, AsyncStorage } from 'react-native'

import firebase from '../../config/firebaseConfig'

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props)
  }

  async componentWillMount() {
    this.props.navigation.navigate('AuthApp')
    // firebase.auth().onAuthStateChanged(user => {
    //   console.log(user)
    //   if (user) this.props.navigation.navigate('AuthApp')
    //   else this.props.navigation.navigate('UnauthApp')
    // })
  }

  render() {
    return (
      <View
        style={{
          backgroundColor: '#6DB3BF',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <ActivityIndicator color="#fff" size="large" />
      </View>
    )
  }
}

export default AuthLoadingScreen

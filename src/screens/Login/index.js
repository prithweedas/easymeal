import React from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  KeyboardAvoidingView,
  ActivityIndicator,
  StatusBar,
  AsyncStorage
} from 'react-native'
import Expo, { Constants } from 'expo'

import Input from '../../Components/Input'
import styles from './styles'
import firebase from '../../config/firebaseConfig'
import fb from 'firebase'
import SubmitButton from '../../Components/Button'

class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null
  }

  state = {
    email: '',
    password: '',
    loading: false,
    error: null
  }

  login = () => {
    this.setState({ loading: true, error: null })
    const { email, password } = this.state
    if (email === '' && password === '')
      return this.setState({
        loading: false,
        error: 'Email & Password are required'
      })
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(console.log)
      .catch(err => this.setState({ loading: false, error: err.message }))
  }

  fblogin = async () => {
    this.setState({ loading: true, error: null })
    try {
      var { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync(
        '1903302189764026',
        { permissions: ['public_profile'] }
      )
    } catch (err) {
      return this.setState({ loading: false, error: 'Something went wrong!' })
    }
    if (type === 'success') {
      const credential = fb.auth.FacebookAuthProvider.credential(token)
      firebase
        .auth()
        .signInAndRetrieveDataWithCredential(credential)
        .then(data => AsyncStorage.setItem('user', data))
        .catch(err => this.setState({ loading: false, error: err.message }))
    } else {
      return this.setState({ loading: false, error: 'Something went wrong!' })
    }
  }

  handelInputChange = (field, text) => this.setState({ [field]: text })

  render() {
    return this.state.loading ? (
      <View
        style={{
          backgroundColor: '#6DB3BF',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <StatusBar backgroundColor="#6DB3BF" barStyle="light-content" />
        <ActivityIndicator color="#fff" size="large" />
      </View>
    ) : (
      <KeyboardAvoidingView style={styles.container}>
        <StatusBar backgroundColor="#6DB3BF" barStyle="light-content" />

        <View
          style={{
            height: 10,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 10,
            alignContent: 'center',
            width: '100%'
          }}
        >
          {this.state.error && (
            <Text style={{ width: '90%', color: 'red', textAlign: 'center' }}>
              {this.state.error}
            </Text>
          )}
        </View>
        <Input
          field="email"
          handelChange={this.handelInputChange}
          style={styles.input}
          placeholder="Email"
          value={this.state.email}
        />
        <Input
          field="password"
          secureTextEntry
          handelChange={this.handelInputChange}
          style={styles.input}
          placeholder="Password"
          value={this.state.password}
        />
        {/* <Button onPress={this.login} title="Login" /> */}
        <SubmitButton
          style={{ marginTop: 5 }}
          text="Login"
          onPress={this.login}
        />
        <TouchableOpacity onPress={this.fblogin}>
          <Text style={styles.loginWithFacebook}>Login with facebook</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            marginTop: 10,
            borderBottomColor: '#fff',
            borderBottomWidth: 1
          }}
          onPress={() => this.props.navigation.push('Signup')}
        >
          <Text style={{ color: '#fff' }}>New User? register</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    )
  }
}

export default LoginScreen

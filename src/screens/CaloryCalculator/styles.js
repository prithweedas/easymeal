import { StyleSheet } from 'react-native'
import { Constants } from 'expo'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#6DB3BF'
  },
  input: {
    width: '90%',
    paddingTop: '3%',
    paddingBottom: '3%',
    paddingLeft: '4%',
    paddingRight: '4%'
  },
  statusBar: {
    height: Constants.statusBarHeight
  },
  formGroup: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'flex-end'
  },
  formItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

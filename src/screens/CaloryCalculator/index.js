import React from 'react'
import {
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  Picker,
  StatusBar,
  Clipboard,
  ToastAndroid
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import RadioGroup from 'react-native-radio-buttons-group'

import styles from './styles'
import Input from '../../Components/Input'
import SubmitButton from '../../Components/Button'

const malePaMap = {
  pa1: 1.0,
  pa2: 1.12,
  pa3: 1.27,
  pa4: 1.54
}
const femalePaMap = {
  pa1: 1.0,
  pa2: 1.14,
  pa3: 1.27,
  pa4: 1.45
}

class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null
  }

  state = {
    height: '',
    weight: '',
    age: '',
    pa: 'pa1',
    data: [
      {
        label: 'Male',
        value: 'male',
        selected: true
      },
      {
        label: 'Female',
        value: 'female'
      }
    ],
    cal: ''
  }
  constructor(props) {
    super(props)
    this.inputs = {}
  }

  focusNextField = key => this.inputs[key].focus()
  handelInputChange = (field, text) => this.setState({ [field]: text })
  onPress = data => this.setState({ data })

  calculate = () => {
    const height = parseFloat(this.state.height)
    const weight = parseFloat(this.state.weight)
    const age = parseFloat(this.state.age)
    const gender = this.state.data.find(item => item.selected)
    const pa =
      gender.value === 'male'
        ? malePaMap[this.state.pa]
        : femalePaMap[this.state.pa]

    const tee =
      gender.value === 'male'
        ? 864 - 9.72 * age + pa * (14.2 * weight + 503 * height)
        : 387 - 7.31 * age + pa * (10.9 * weight + 660.7 * height)

    this.setState({ cal: `${tee}` })
    Clipboard.setString(`${tee}`)
    ToastAndroid.showWithGravityAndOffset(
      'Result copied to clipboard',
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      0,
      100
    )
  }
  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        <StatusBar backgroundColor="#6DB3BF" barStyle="light-content" />
        {/* <View style={styles.statusBar} /> */}
        <View>
          <TouchableOpacity
            style={{ marginLeft: 5 }}
            onPress={this.props.navigation.toggleDrawer}
          >
            <Icon name="md-menu" color="#fff" size={40} />
          </TouchableOpacity>
        </View>
        <View>
          {/* <View style={styles.formGroup}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-start'
              }}
            >
              <Text
                style={{
                  width: '90%',
                  paddingLeft: '5%',
                  paddingRight: '4%',
                  color: '#fff'
                }}
              >
                Height
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            />
          </View> */}
          <View style={styles.formGroup}>
            <View style={styles.formItem}>
              <Input
                setref={input => {
                  this.inputs['height'] = input
                }}
                returnKeyType={'next'}
                blurOnSubmit={false}
                field="height"
                handelChange={this.handelInputChange}
                style={styles.input}
                placeholder="Height in meters"
                value={this.state.height}
                keyboardType="numeric"
                onSubmitEditing={() => {
                  this.focusNextField('weight')
                }}
              />
            </View>
            <View style={styles.formItem}>
              <Input
                setref={input => {
                  this.inputs['weight'] = input
                }}
                blurOnSubmit={false}
                field="weight"
                handelChange={this.handelInputChange}
                style={styles.input}
                placeholder="Weight in Kg"
                value={this.state.weight}
                keyboardType="numeric"
                onSubmitEditing={() => {
                  this.focusNextField('age')
                }}
                returnKeyType={'next'}
              />
            </View>
          </View>
          <View style={styles.formGroup}>
            <View style={styles.formItem}>
              <Input
                setref={input => {
                  this.inputs['age'] = input
                }}
                returnKeyType={'done'}
                blurOnSubmit={false}
                field="age"
                handelChange={this.handelInputChange}
                style={styles.input}
                placeholder="Age"
                value={this.state.age}
                keyboardType="numeric"
              />
            </View>
            <View style={styles.formItem}>
              <Picker
                style={{
                  width: '90%',
                  height: 48,
                  backgroundColor: '#fff',
                  marginVertical: 10,
                  borderRadius: 4
                }}
                onValueChange={value => this.setState({ pa: value })}
                selectedValue={this.state.pa}
              >
                <Picker.Item label="Sedentary" value="pa1" />
                <Picker.Item label="Low active" value="pa2" />
                <Picker.Item label="Active" value="pa3" />
                <Picker.Item label="Very active" value="pa4" />
              </Picker>
            </View>
          </View>
          <RadioGroup
            flexDirection="row"
            radioButtons={this.state.data}
            onPress={this.onPress}
          />
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Input
              field="cal"
              style={styles.input}
              placeholder="Calories"
              value={this.state.cal}
            />
          </View>
        </View>

        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <SubmitButton
            style={{ marginTop: 5 }}
            text="Calculate"
            onPress={this.calculate}
            style={{ width: '50%' }}
          />
        </View>
      </KeyboardAvoidingView>
    )
  }
}

export default LoginScreen

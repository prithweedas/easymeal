import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  Switch,
  StatusBar
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

import styles from './styles'
import Input from '../../Components/Input'
import SubmitButton from '../../Components/Button'

class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  }

  state = {
    CalInputValue: '',
    NumMealsValue: '',
    CarbInputValue: '',
    ProteinInputValue: '',
    FatInputValue: '',
    NumVeggiesValue: '',
    BigBreakFastValue: false,
    WorkoutDayValue: false,
    NoCarbsValue: false,
    MetricUnitsValue: false,
    DietSelectorValue: ''
  }
  constructor(props) {
    super(props)
    this.inputs = {}
  }

  focusNextField = key => this.inputs[key].focus()
  handelInputChange = (field, text) => this.setState({ [field]: text })

  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        <StatusBar backgroundColor="#6DB3BF" barStyle="light-content" />
        {/* <View style={styles.statusBar} /> */}
        <View>
          <TouchableOpacity
            style={{ marginLeft: 5 }}
            onPress={this.props.navigation.toggleDrawer}
          >
            <Icon name="md-menu" color="#fff" size={40} />
          </TouchableOpacity>
        </View>
        <View>
          <View style={styles.formGroup}>
            <View style={styles.formItem}>
              <Input
                setref={input => {
                  this.inputs['cal'] = input
                }}
                returnKeyType={'next'}
                blurOnSubmit={false}
                field="CalInputValue"
                handelChange={this.handelInputChange}
                style={styles.input}
                placeholder="Cal"
                value={this.state.CalInputValue}
                keyboardType="numeric"
                onSubmitEditing={() => {
                  this.focusNextField('meals')
                }}
              />
            </View>
            <View style={styles.formItem}>
              <Input
                setref={input => {
                  this.inputs['meals'] = input
                }}
                blurOnSubmit={false}
                field="NumMealsValue"
                handelChange={this.handelInputChange}
                style={styles.input}
                placeholder="Meals"
                value={this.state.NumMealsValue}
                keyboardType="numeric"
                onSubmitEditing={() => {
                  this.focusNextField('carb')
                }}
                returnKeyType={'next'}
              />
            </View>
          </View>
          <View style={styles.formGroup}>
            <View style={styles.formItem}>
              <Input
                setref={input => {
                  this.inputs['carb'] = input
                }}
                returnKeyType={'next'}
                blurOnSubmit={false}
                field="CarbInputValue"
                handelChange={this.handelInputChange}
                style={styles.input}
                placeholder="Carb"
                value={this.state.CarbInputValue}
                keyboardType="numeric"
                onSubmitEditing={() => {
                  this.focusNextField('protien')
                }}
              />
            </View>
            <View style={styles.formItem}>
              <Input
                setref={input => {
                  this.inputs['protien'] = input
                }}
                blurOnSubmit={false}
                field="ProteinInputValue"
                handelChange={this.handelInputChange}
                style={styles.input}
                placeholder="Protein"
                value={this.state.ProteinInputValue}
                keyboardType="numeric"
                onSubmitEditing={() => {
                  this.focusNextField('fat')
                }}
              />
            </View>
          </View>
          <View style={styles.formGroup}>
            <View style={styles.formItem}>
              <Input
                setref={input => {
                  this.inputs['fat'] = input
                }}
                returnKeyType={'next'}
                blurOnSubmit={false}
                field="FatInputValue"
                handelChange={this.handelInputChange}
                style={styles.input}
                placeholder="Fat"
                value={this.state.FatInputValue}
                keyboardType="numeric"
                onSubmitEditing={() => {
                  this.focusNextField('veg')
                }}
              />
            </View>
            <View style={styles.formItem}>
              <Input
                setref={input => {
                  this.inputs['veg'] = input
                }}
                blurOnSubmit={true}
                field="NumVeggiesValue"
                handelChange={this.handelInputChange}
                style={styles.input}
                placeholder="Veggies"
                value={this.state.NumVeggiesValue}
                keyboardType="numeric"
                returnKeyType={'done'}
              />
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'space-around'
          }}
        >
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#fff' }}>Big BreakFast</Text>
            <Switch
              onTintColor="#47D98B"
              tintColor="#FF5D4E"
              onValueChange={value =>
                this.setState({ BigBreakFastValue: value })
              }
              value={this.state.BigBreakFastValue}
            />
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#fff' }}>Workout Day</Text>
            <Switch
              onTintColor="#47D98B"
              tintColor="#FF5D4E"
              onValueChange={value => this.setState({ WorkoutDayValue: value })}
              value={this.state.WorkoutDayValue}
            />
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#fff' }}>No Carbs</Text>
            <Switch
              onTintColor="#47D98B"
              tintColor="#FF5D4E"
              onValueChange={value => this.setState({ NoCarbsValue: value })}
              value={this.state.NoCarbsValue}
            />
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#fff' }}>Metric Unit</Text>
            <Switch
              onTintColor="#47D98B"
              tintColor="#FF5D4E"
              onValueChange={value =>
                this.setState({ MetricUnitsValue: value })
              }
              value={this.state.MetricUnitsValue}
            />
          </View>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Input
            field="DietSelectorValue"
            handelChange={this.handelInputChange}
            style={styles.input}
            placeholder="Diet Selector"
            value={this.state.DietSelectorValue}
            returnKeyType={'done'}
          />
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <SubmitButton
            style={{ marginTop: 5 }}
            text="Submit"
            onPress={() => {}}
            style={{ width: '50%' }}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 20,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('CalorieCalc')}
          >
            <Text
              style={{
                color: '#fff',
                borderBottomColor: '#fff',
                borderBottomWidth: 1
              }}
            >
              Not sure how much Calorie you need?
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

export default HomeScreen

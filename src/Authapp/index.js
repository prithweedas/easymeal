import { createDrawerNavigator } from 'react-navigation'

import HomeScreen from '../screens/Home'
import CalorieCalc from '../screens/CaloryCalculator'
import Logout from '../Components/Logout'

export default createDrawerNavigator({
  Home: HomeScreen,
  CalorieCalc: {
    screen: CalorieCalc,
    navigationOptions: ({ navigation }) => ({
      title: `Calorie Calculator`
    })
  },
  Logout: Logout
})

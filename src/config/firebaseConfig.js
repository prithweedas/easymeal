import firebase from 'firebase'

const config = {
  apiKey: 'AIzaSyC5Ej30iQcq30AAEOOUFebkqQYF4M9wrLc',
  authDomain: 'easymeals-7c7b7.firebaseapp.com',
  databaseURL: 'https://easymeals-7c7b7.firebaseio.com',
  projectId: 'easymeals-7c7b7',
  storageBucket: 'easymeals-7c7b7.appspot.com',
  messagingSenderId: '202212171433'
}

export default (!firebase.apps.length
  ? firebase.initializeApp(config)
  : firebase.app())

import React from 'react'
import { Text } from 'react-native'

import firebase from '../../config/firebaseConfig'

export default class Logout extends React.Component {
  componentWillMount() {
    firebase.auth().signOut()
  }
  render() {
    return <Text>Logout</Text>
  }
}

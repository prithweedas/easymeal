import React from 'react'
import { TouchableHighlight, Text, StyleSheet } from 'react-native'
import color from 'color'

const NavButton = ({ onPress, text, style }) => (
  <TouchableHighlight
    underlayColor={'#6DB3BF'}
    onPress={onPress}
    style={[styles.button, style]}
  >
    <Text style={styles.text}>{text}</Text>
  </TouchableHighlight>
)

const styles = StyleSheet.create({
  button: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 5,
    paddingBottom: 5,
    borderWidth: 1,
    borderColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3
  },
  text: {
    fontSize: 20,
    color: '#fff'
  }
})

export default NavButton

import React from 'react'
import { TextInput, View, Dimensions } from 'react-native'

import styles from './styles'

class FormInput extends React.Component {
  render() {
    const { field, handelChange, setref, ...textInputProps } = this.props
    return (
      <View style={styles.container}>
        <TextInput
          ref={setref ? setref : () => {}}
          onChangeText={text => handelChange(field, text)}
          underlineColorAndroid="transparent"
          {...textInputProps}
          style={styles.input}
        />
      </View>
    )
  }
}

export default FormInput

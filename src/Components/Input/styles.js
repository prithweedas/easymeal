import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    width: '90%',
    height: 48,
    backgroundColor: '#fff',
    marginVertical: 10,
    borderRadius: 4
  },
  input: {
    fontSize: 18,
    flex: 1,
    paddingHorizontal: 8,
    color: '#153641'
  }
})

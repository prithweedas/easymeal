import { createStackNavigator } from 'react-navigation'

import LoginScreen from '../screens/Login'
import SignupScreen from '../screens/Signup'

export default createStackNavigator({
  Login: LoginScreen,
  Signup: SignupScreen
})
